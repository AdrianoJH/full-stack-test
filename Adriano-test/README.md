Movie App


Backend
Pré-requisitos
Docker
Docker Compose
Construindo e executando o backend
Certifique-se de estar na raiz do projeto.

Execute o seguinte comando para iniciar o banco de dados e o backend:


docker-compose up
Isso iniciará o banco de dados PostgreSQL e o backend Express.

executar docker exec -it movie_container /bin/bash para acessar o container do app.
Executar npx prisma migrate dev dentro do container app para executar as migrações.

O backend estará acessível em http://localhost:3000.
Frontend
Pré-requisitos
Node.js (versão 14 ou superior)
Construindo e executando o frontend
Certifique-se de estar na raiz do projeto.

Navegue para o diretório do frontend:



cd frontend
Instale as dependências:

npm install
Inicie o servidor de desenvolvimento:

npm run dev
Isso iniciará o servidor de desenvolvimento Vite.

O frontend estará acessível em http://localhost:3000.
Compilar o frontend para produção
Para compilar o frontend para produção, execute o seguinte comando:


npm run build
Isso irá gerar os arquivos de build na pasta dist, prontos para implantação.#   m o v i e S t a r  
 #   m o v i e - s t a r  
 