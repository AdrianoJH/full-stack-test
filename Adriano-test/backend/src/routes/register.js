import express from 'express';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcryptjs';

const prisma = new PrismaClient(); // Inicializando o cliente Prisma para acessar o banco de dados
const router = express.Router(); // Inicializando um novo roteador Express


router.post('/', async (req, res) => {
    const { email, password, userName, lastName } = req.body;

    console.log('Rota de registro acessada');
    console.log('Dados recebidos:', { email, password, userName, lastName });

    // Verifica se o usuário já existe
    const existingUser = await prisma.user.findUnique({ where: { email } });
    if (existingUser) {
        return res.status(400).json({ message: 'Usuário já registrado' });
    }

    // Hash da senha usando bcryptjs
    const hashedPassword = await bcrypt.hash(password, 10);

    // Cria o novo usuário
    const newUser = await prisma.user.create({
        data: { email, password: hashedPassword, name: userName, lastName }
    });

    res.json({ message: 'Usuário registrado com sucesso', user: newUser });
});

export default router;