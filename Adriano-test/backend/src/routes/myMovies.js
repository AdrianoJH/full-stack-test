import express from 'express';
import { PrismaClient } from '@prisma/client';
import path from 'path';

const prisma = new PrismaClient(); // Inicializando o cliente Prisma para interagir com o banco de dados
const router = express.Router(); // Criando um novo roteador Express para lidar com as rotas relacionadas aos estabelecimentos


router.get('/', async (req, res) => {
    try {
        const userId = req.userId; // Obtém o ID do usuário autenticado
        const movies = await prisma.movie.findMany({
            where: {
                users: {
                    some: {
                        id: userId
                    }
                }
            }
        });
        // Modificar o caminho das imagens para ser acessível publicamente
        const moviesWithPublicImagePath = movies.map(movie => ({
            ...movie,
            image: `${req.protocol}://${req.get('host')}/files/${path.basename(movie.image)}`
        }));
        res.json(moviesWithPublicImagePath);
    } catch (error) {
        res.status(500).json({ message: 'Erro ao buscar os filmes do usuário', error: error.message });
    }
});

export default router;