import express from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import loginRouter from './login.js';
import registerRouter from './register.js';
import logoutRouter from './logout.js';
import moviesRouter from './movies.js';
import moviesProtectedRouter from './moviesProtected.js';
import myMoviesRouter from './myMovies.js';
import userRouter from './user.js';

dotenv.config();

const router = express.Router();

const secretKey = process.env.JWT_SECRET;
// Middleware para verificar o token
const verifyToken = (req, res, next) => {
    const token = req.headers.authorization?.split(' ')[1];

    if (!token) return res.status(401).json({ message: 'Token não fornecido' });

    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            return res.status(403).json({ message: 'Falha na autenticação do token' });
        }
        req.userId = decoded.id;
        next();
    });
};

router.use('/register', registerRouter);
router.use('/login', loginRouter);
router.use('/logout', logoutRouter);
router.use('/movies', moviesRouter);

router.use(verifyToken);

router.use('/user', userRouter);
router.use('/my-movies', myMoviesRouter);
router.use('/movies', moviesProtectedRouter);

export default router;