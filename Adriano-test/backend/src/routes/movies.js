import express from 'express';
import { PrismaClient } from '@prisma/client';
import path from 'path';

const prisma = new PrismaClient(); // Inicializando o cliente Prisma para interagir com o banco de dados
const router = express.Router(); // Criando um novo roteador Express para lidar com as rotas relacionadas aos estabelecimentos



router.get('/', async (req, res) => {
    try {
        const movies = await prisma.movie.findMany();
        // Modificar o caminho das imagens para ser acessível publicamente
        const moviesWithPublicImagePath = movies.map(movie => ({
            ...movie,
            image: `${req.protocol}://${req.get('host')}/files/${path.basename(movie.image)}`
        }));
        res.json(moviesWithPublicImagePath);
    } catch (error) {
        res.status(500).json({ message: 'Erro ao buscar os filmes', error: error.message });
    }
});

router.get('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const movie = await prisma.movie.findUnique({
            where: { id: parseInt(id) }
        });
        if (!movie) {
            return res.status(404).json({ message: 'Filme não encontrado' });
        }
        // Modificar o caminho da imagem para ser acessível publicamente
        const movieWithPublicImagePath = {
            ...movie,
            image: `${req.protocol}://${req.get('host')}/files/${path.basename(movie.image)}`
        };
        res.json(movieWithPublicImagePath);
    } catch (error) {
        res.status(500).json({ message: 'Erro ao buscar os detalhes do filme', error: error.message });
    }
});

router.get('/:id/ratings', async (req, res) => {
    try {
        const { id } = req.params;
        const ratings = await prisma.movieRating.findMany({
            where: {
                movieId: parseInt(id)
            },
            include: {
                user: true // Inclui informações do usuário que fez a avaliação
            }
        });
        res.json(ratings);
    } catch (error) {
        console.error('Erro ao buscar as avaliações do filme:', error);
        res.status(500).json({ message: 'Erro ao buscar as avaliações do filme', error: error.message });
    }
});

export default router; 