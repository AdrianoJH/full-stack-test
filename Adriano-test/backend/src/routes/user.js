import express from 'express';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcryptjs';

const prisma = new PrismaClient(); // Inicializando o cliente Prisma para interagir com o banco de dados
const router = express.Router(); // Criando um novo roteador Express para lidar com as rotas relacionadas aos estabelecimentos


router.get('/', async (req, res) => {
    const user = await prisma.user.findUnique({ where: { id: req.userId } });
    res.json(user);
});

// Rota para editar informações do usuário
router.put('/', async (req, res) => {
    const userId = req.userId; // Obtém o ID do usuário autenticado
    const { name, lastName, email, password } = req.body;
    try {
        const updatedUserData = { name, lastName, email };
        if (password) {
            const hashedPassword = await bcrypt.hash(password, 10);
            updatedUserData.password = hashedPassword;
        }

        const updatedUser = await prisma.user.update({
            where: { id: userId },
            data: updatedUserData
        });
        res.json(updatedUser);
    } catch (error) {
        console.error('Erro ao atualizar usuário:', error);
        res.status(500).json({ message: 'Erro ao atualizar usuário', error: error.message });
    }
});

// Rota para excluir usuário
router.delete('/', async (req, res) => {
    const userId = req.userId; // Obtém o ID do usuário autenticado
    try {
        await prisma.user.delete({
            where: { id: userId }
        });
        res.json({ message: 'Usuário excluído com sucesso' });
    } catch (error) {
        console.error('Erro ao excluir usuário:', error);
        res.status(500).json({ message: 'Erro ao excluir usuário', error: error.message });
    }
});

export default router; 
