import express from 'express';
import { PrismaClient } from '@prisma/client'
import multer from 'multer'; // Importe o multer
import path from 'path';

const prisma = new PrismaClient(); // Inicializando o cliente Prisma para interagir com o banco de dados
const router = express.Router(); // Criando um novo roteador Express para lidar com as rotas relacionadas aos estabelecimentos


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images') // Diretório onde as imagens serão salvas
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname)) // Nome do arquivo (timestamp + extensão)
    }
});

const upload = multer({ storage: storage });

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const { title, category, description } = req.body;
        const userId = req.userId; // Obtém o ID do usuário que está adicionando o filme
        const imagePath = req.file.path; // Caminho da imagem salva pelo multer
        const movie = await prisma.movie.create({
            data: {
                title,
                category,
                description,
                image: imagePath,
                // Associando o filme ao usuário pelo relacionamento definido no modelo User
                users: { connect: { id: userId } }
            }
        });
        res.json(movie);
    } catch (error) {
        console.error('Erro ao criar filme:', error);
        res.status(500).json({ message: 'Erro ao criar filme', error: error.message });
    }
});

router.put('/:id', upload.single('image'), async (req, res) => {
    const { id } = req.params;
    const { title, category, description } = req.body;
    let updatedMovieData = { title, category, description };

    if (req.file) {
        updatedMovieData.image = req.file.path;
    }

    const updatedMovie = await prisma.movie.update({
        where: { id: parseInt(id) },
        data: updatedMovieData
    });
    res.json(updatedMovie);
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params; // Obtém o ID do filme a ser excluído
    try {
        await prisma.movie.delete({
            where: { id: parseInt(id) }
        });
        res.json({ message: 'Filme excluído com sucesso' });
    } catch (error) {
        console.error('Erro ao excluir filme:', error);
        res.status(500).json({ message: 'Erro ao excluir filme', error: error.message });
    }
});

router.post('/:id/ratings', async (req, res) => {
    const { id } = req.params;
    const { rating, comment } = req.body;
    const userId = req.userId; // Obtém o ID do usuário autenticado

    try {
        const movie = await prisma.movie.findUnique({ where: { id: parseInt(id) } });
        if (!movie) {
            return res.status(404).json({ message: 'Filme não encontrado' });
        }

        // Cria a avaliação
        const ratings = await prisma.movieRating.create({
            data: {
                rating,
                comment,
                userId,
                movieId: parseInt(id)
            }
        });
        res.json(ratings);
    } catch (error) {
        console.error('Erro ao adicionar avaliação:', error);
        res.status(500).json({ message: 'Erro ao adicionar avaliação', error: error.message });
    }
});

router.get('/:id/ratings/me', async (req, res) => {
    const userId = req.userId; // Obtém o ID do usuário autenticado
    const movieId = req.params.id; // Obtém o ID do filme da URL

    try {
        const userRating = await prisma.movieRating.findUnique({
            where: {
                userId_movieId: {
                    userId: userId,
                    movieId: parseInt(movieId),
                },
            },
        });

        res.json({ hasRated: !!userRating });
    } catch (error) {
        console.error('Erro ao verificar a avaliação do usuário para o filme:', error);
        res.status(500).json({ message: 'Erro ao verificar a avaliação do usuário para o filme' });
    }
});


export default router;