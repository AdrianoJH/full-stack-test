import express from 'express';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv'

dotenv.config(); 

const prisma = new PrismaClient(); // Inicializando o cliente Prisma para interagir com o banco de dados
const router = express.Router(); // Inicializando um roteador Express

const secretKey = process.env.JWT_SECRET; 

router.post('/', async (req, res) => {
    const { email, password } = req.body;
    const user = await prisma.user.findUnique({ where: { email } });

    if (!user) return res.status(404).json({ message: 'Usuário não encontrado' });

    // Comparação da senha usando bcryptjs
    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) return res.status(401).json({ message: 'Senha incorreta' });

    const token = jwt.sign({ id: user.id }, secretKey, { expiresIn: '8760000h' });
    res.json({ token });
});

export default router;