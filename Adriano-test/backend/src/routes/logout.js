import express from 'express';

const router = express.Router();

router.post('/', (req, res) => {
    res.clearCookie('token'); // Limpa o token do cookie
    res.json({ message: 'Logout bem-sucedido' });
});

export default router;