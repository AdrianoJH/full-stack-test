import express from 'express';
import routes from './routes/index.js';
import cors from 'cors';
import path from 'path';

const app = express();
const PORT = process.env.PORT || 3000;

const publicImagesPath = path.join(process.cwd(), 'public', 'images');

// Middleware para tratar o corpo das requisições
app.use(express.json());

app.use(cors());

app.use('/files', express.static(publicImagesPath));

// Define o uso das rotas
app.use('/api', routes);

app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});
