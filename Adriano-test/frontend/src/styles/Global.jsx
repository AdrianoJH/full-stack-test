import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
 * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: Arial, Helvetica, sans-serif;
    }
    
    body {
        width: 100%;
        min-height: 100vh;
        overflow-x: hidden ;
        overflow-y: auto;
        background: linear-gradient(180deg, #1f1f23 10%, #414147 90%);
    }
`;

export default GlobalStyle;