import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Home from '../views/Home/Home';
import LoginPage from '../views/LoginPage/LoginPage';
import RegisterPage from '../views/RegisterPage/RegisterPage';
import MyMoviesPage from '../views/MyMoviesPage/MyMoviesPage';
import AccountsPage from '../views/AccountsPage/AccountsPage';
import AddMoviePage from '../views/AddMoviePage/AddMoviePage';
import MovieDetail from '../views/MovieDetail/MovieDetail';
import EditMoviePage from '../views/EditMoviePage/EditMoviePage';

const Routers = ({ loggedIn, setUserName, setLoggedIn, userId, handleLogout, averageRatings, searchTerm, setSearchTerm}) => {
  return (
    <Routes>
      <Route path="/" element={<Home searchTerm={searchTerm} setSearchTerm={setSearchTerm} userId={userId}/>} />
      <Route path="/login" element={<LoginPage loggedIn={loggedIn} setUserName={setUserName} setLoggedIn={setLoggedIn} />} />
      <Route path="/register" element={<RegisterPage />} />
      <Route path="/movie" element={<MyMoviesPage loggedIn={loggedIn} userId={userId} searchTerm={searchTerm}/>} />
      <Route path="/accounts" element={<AccountsPage handleLogout={handleLogout}/>} />
      <Route path="/add-movie" element={<AddMoviePage />} />
      <Route path="/movie-detail/:id" element={<MovieDetail userId={userId} averageRatings={averageRatings}/>} />
      <Route path="/edit-movie/:id" element={<EditMoviePage />} />
    </Routes>
  );
};

export default Routers;
