import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    gap: 40px;
    height: 100%;
    width: 85%;
    padding: 30px 0;

    
    h1 {
        text-align: start;
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
    }

    @media (max-width: 768px) {
        width: 90%;
    }

`;

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    justify-content: center;
    padding: 30px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset, -8px 8px 16px 0px rgb(20, 20, 20);
    background-color: #4e4e51;

    h2 {
        text-align: start;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
    }

    p {
        margin-top: 10px;
        text-align: start;
        font-size: 16px;
        color: #d1d1d1;
    }

    form {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        width: 100%;
        height: 100%;

        label {
            margin-top: 20px;
            margin-bottom: 5px;
            text-align: start;
            font-size: 16px;
            font-weight: bold;
            color: #d1d1d1;
        }

        textarea {
            width: 45%;
            padding: 10px 10px;
            border-radius: 8px;
            box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
            background-color: #1f1f23;
            font-size: 16px;
            font-weight: bold;
            border: none;
            color: #d1d1d1;
            background-color: #1f1f23;

            &:focus {
                outline: none;
            }
        }

        button {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 150px;
            height: 40px;
            margin-top: 20px;
            background-color: #1f1f23;
            color: #d1d1d1;
            font-size: 20px;
            font-weight: bold;
            border: none;
            border-radius: 8px;
            box-shadow: #000000af 0px 10px 10px;
            cursor: pointer;

            &:hover {
                background-color:  #2200A8;
            
                }

        }
    }
`;

export const BoxSelect = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    width: 45%;
    height: 40px;
    border-radius: 8px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
    background-color: #1f1f23;

    select {
        width: 100%;
        height: 100%;
        padding: 0 10px;
        font-size: 16px;
        font-weight: bold;
        border: none;
        color: #d1d1d1;
        background: transparent;
        appearance: none;
        cursor: pointer;

        &:focus {
            outline: none;
        }

        option {
            font-size: 16px;
            font-weight: bold;
            color: #d1d1d1;
            box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
            background-color: #1f1f23;

            &:active {
                background-color:  #2200A8 !important;
            }
        }
    }

    span {
        position: absolute;
        top: 7px;
        right: 5px;
        font-size: 20px;
        font-weight: bold;
        color: #d1d1d1;
        pointer-events: none;
        cursor: pointer;
    }
`;

export const AvaliationContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;

    ul {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        width: 100%;
        height: 100%;

        li {
            display: flex;
            flex-direction: column;
            gap: 10px;
            list-style: none;

            p {
                font-size: 16px;
                font-weight: bold;
                color: #d1d1d1;

                &.name {
                    color:  #2200A8
                }
            }

            strong {
                font-size: 16px;
                font-weight: 400;
                color: #d1d1d1bb;

                svg {
                    font-size: 18px;
                    color:  #2200A8
                }
            }
        }

        hr {
            width: 100%;
            height: 2px;
            margin: 20px 0;
            background-color: transparent;
            border: none;
            border-bottom: 1px solid #d1d1d138;
        }
    }
`;