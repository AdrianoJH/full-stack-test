import React, { useState, useEffect } from 'react';
import { IoMdArrowDropdown } from "react-icons/io";
import { Container, Content, BoxSelect, AvaliationContainer } from './AvaliationStyle';
import { addMovieRating, getMovieRatings, hasUserRatedMovie } from '../../services/api'; // Importe a função para buscar avaliações
import { FaStar } from 'react-icons/fa';

function Avaliation({ movieId }) {
  const [rating, setRating] = useState('');
  const [comment, setComment] = useState('');
  const [ratings, setRatings] = useState([]);
  const [userHasRated, setUserHasRated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const hasRated = await hasUserRatedMovie(movieId);
        setUserHasRated(hasRated);
        const ratingsData = await getMovieRatings(movieId);
        setRatings(ratingsData);
      } catch (error) {
        console.error('Erro ao buscar as avaliações do filme:', error);
      }
    };
    fetchData();
  }, [movieId]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const ratingInt = parseInt(rating);
      await addMovieRating(movieId, { rating: ratingInt, comment });
      alert('Avaliação adicionada com sucesso');
      setRating('');
      setComment('');
    } catch (error) {
      console.error('Erro ao adicionar avaliação:', error);
      alert('Erro ao adicionar avaliação');
    }
  };

  return (
    <Container>
      <h1>Avaliações</h1>
      {!userHasRated && (
        <>
          <Content>
            <h2>Envie sua avaliação</h2>
            <p>Preencha o formulário com a nota e comentário sobre o filme.</p>
            <form onSubmit={handleSubmit}>
              <label htmlFor="note">Nota do filme</label>
              <BoxSelect>
                <select name="nota" id="note" required value={rating} onChange={(e) => setRating(e.target.value)}>
                  <option value="">Selecione</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
                <span><IoMdArrowDropdown /></span>
              </BoxSelect>
              <label htmlFor="coments">Comentário</label>
              <textarea name="Comentário" id="coments" rows="5" placeholder='O que você achou do filme?' required value={comment} onChange={(e) => setComment(e.target.value)}></textarea>
              <button type='submit'>Enviar</button>
            </form>
          </Content>
        </>
      )}
      <AvaliationContainer>
        {ratings.map((rating, index) => (
          <ul>
            <li key={index}>
              <p className='name'>{rating.user.name} {rating.user.lastName}</p>
              <p><strong><FaStar /></strong> {rating.rating}</p>
              <strong>Comentário:</strong>
              <p> {rating.comment}</p>
            </li>
            {index !== ratings.length - 1 && <hr />}
          </ul>
        ))}
      </AvaliationContainer>
    </Container>
  );
}

export default Avaliation;
