import React from 'react';
import { MdCancel, MdClose } from "react-icons/md";
import { FaCheck } from "react-icons/fa6";
import { Container, ModalContent, BoxBtn, BtnConfirm, BtnCancel, BoxMsg } from './ModalDelStyle';

const ModalDel = ({ showDeleteButton, onDelete, setShowModal }) => {
    const confirmDelete = () => {
        onDelete(); // Chama a função onDelete passada como propriedade
        setShowModal(false); // Fecha o modal após a exclusão
    };

    const cancelDelete = () => {
        setShowModal(false); // Fecha o modal sem executar a exclusão
    };

    return (
        <Container>
            <ModalContent>
                <MdClose onClick={cancelDelete} />
                <BoxMsg>
                    <span>Tem Certeza que deseja excluir?</span>
                    <span>Essa ação será irreversível.</span>
                </BoxMsg>
                <BoxBtn>
                    {showDeleteButton && ( // Renderiza o botão de exclusão apenas se showDeleteButton for true
                        <BtnConfirm onClick={confirmDelete}> {/* Chama a função confirmDelete ao clicar */}
                            <FaCheck />
                            Confirmar
                        </BtnConfirm>
                    )}
                    <BtnCancel onClick={cancelDelete}> {/* Chama a função cancelDelete ao clicar */}
                        <MdCancel />
                        Cancelar
                    </BtnCancel>
                </BoxBtn>
            </ModalContent>
        </Container>
    )
}

export default ModalDel;
