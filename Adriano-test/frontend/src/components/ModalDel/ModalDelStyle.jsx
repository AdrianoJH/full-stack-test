import styled from 'styled-components';

export const Container = styled.div`
    position: fixed;
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    top: 0;
    justify-content: center;
    align-items: center;
    background-color: #000000b4;

    @media (max-width: 768px) {
        width: 100%;
        top: 0;
    }

`;

export const ModalContent = styled.div`
    position: fixed;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    gap: 30px;
    width: 400px;
    padding: 20px;
    border-radius: 10px;
    background-color: #9d9d9f;
    box-shadow: #111112 -10px 10px 40px;

    svg {
        font-size: 25px;
        color: #111112;
        cursor: pointer;

        &:hover {
            color: #bb0000;
        }
    }

    @media (max-width: 768px) {
        width: 90%;
    }
`;

export const BoxMsg = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    width: 100%;

    span {
        font-size: 20px;
        font-weight: bold;
        color: #111112;
    }
`;

export const BoxBtn = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
    gap: 20px;
    width: 100%;

`;

export const BtnConfirm = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 10px;
    width: 350px;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 18px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    svg {
        color: #12b200;
    }
    

    &:hover {
        background-color:  #085000;
    }

    @media (max-width: 768px) {
        font-size: 16px;

        svg {
            font-size: 18px;
        }
    }
`;

export const BtnCancel = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 10px;
    width: 350px;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 18px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    svg {
        color: #a40000;
    }
    &:hover {
        background-color:  #590101;
    }

    @media (max-width: 768px) {
        font-size: 16px;

        svg {
            font-size: 18px;
        }
    }
`;