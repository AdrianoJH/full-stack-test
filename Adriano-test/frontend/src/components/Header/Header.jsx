import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../img/logo.svg';
import { FaBars, FaPlus } from "react-icons/fa";
import { IoSearchOutline } from "react-icons/io5";
import { Container, MenuNav, Nav, InputContainer, MobileMenu } from './HeaderStyle';
import { FaXmark } from 'react-icons/fa6';

const Header = ({ loggedIn, userName, handleLogout, setSearchTerm }) => {
  const [isMenuVisible, setIsMenuVisible] = useState(false);

  const toggleMenu = () => {
    setIsMenuVisible(!isMenuVisible);
  }

  return (
    <Container>
      <MenuNav>
        <Nav className={loggedIn ? 'nav-logged-in' : 'nav-logged-out'}>
          <Link to='/'>
            <img src={Logo} alt="logo" />
            <h1>MovieStar</h1>
          </Link>
          <InputContainer>
            <input type="text" placeholder='Buscar Filmes' onChange={(e) => setSearchTerm(e.target.value)} />
            <IoSearchOutline />
          </InputContainer>
        </Nav>
        <ul className={loggedIn ? 'ul-logged-in' : 'ul-logged-out'}>
          {loggedIn && (
            <>
              <li><Link to='/add-movie'><FaPlus />Incluir Filme</Link></li>
              <li><Link to='/movie'>Meus Filmes</Link></li>
              <li><Link to='/accounts'>{userName}</Link></li>
              <li><Link onClick={handleLogout}>Sair</Link></li>
            </>
          )}
          {!loggedIn && (
            <li><Link to='/login'>Login</Link></li>
          )}
        </ul>
      </MenuNav>
      <div id='button'>
        <button id='btn-menu-mobile' onClick={toggleMenu}>{isMenuVisible ? <FaXmark id='close' /> : <FaBars id='open' />}</button>
      </div>
      <MobileMenu className={isMenuVisible ? 'visible' : 'hidden'}>
        <ul>
        {loggedIn && (
            <>
              <li onClick={() => toggleMenu(isMenuVisible)}><Link to='/add-movie'><FaPlus className='icon-plus'/>Incluir Filme</Link></li>
              <li onClick={() => toggleMenu(isMenuVisible)}><Link to='/movie'>Meus Filmes</Link></li>
              <li onClick={() => toggleMenu(isMenuVisible)}><Link to='/accounts'>{userName}</Link></li>
              <li onClick={() => toggleMenu(isMenuVisible)}><Link onClick={handleLogout}>Sair</Link></li>
            </>
          )}
          {!loggedIn && (
            <li onClick={() => toggleMenu(isMenuVisible)}><Link to='/login'>Login</Link></li>
          )}
        </ul>
      </MobileMenu>
    </Container>
  )
}

export default Header;
