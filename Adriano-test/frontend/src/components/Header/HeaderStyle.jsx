import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 90px;
    padding: 0 20px;
    box-shadow: 0 5px 15px 3px #111112;
    background-color: #0f0f12;

    button#btn-mobile {
        display: none;
    }

    .visible {
        display: flex;
        flex-direction: column;
        justify-content: start;
        align-items: start;
        padding-left: 10px;
    }  

    .hidden {
        display: none;
    }   

    div#button {
        display: none;
    }

    @media screen and (max-width: 768px) {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: auto;
        box-shadow: none;
        background-color: transparent;

        div#button {
            display: flex;
            justify-content: start;
            align-items: end;
            width: 100% ;
            height: 10px;
        }

        button#btn-menu-mobile {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 22px;
            height: auto;
            border: none;
            background: none;
            z-index: 99999;

            svg#open {
                position: absolute;
                width: 18px;
                height: 18px;
                top: 18px;
                left:10px;
                color: #d4d4d8;
            }

            svg#close {
                position: absolute;
                width: 18px;
                height: 18px;
                top: 10px;
                left: 5px;
                color: #d4d4d8;            }
        }
    
    }

`;

export const MenuNav = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: 100%;

    ul {
        display: flex;
        justify-content: flex-end;
        gap: 30px;
        width: 35%;

        li {
            display: flex;
            list-style: none;
            gap: 5px;

            a {
                position: relative;
                display: flex;
                gap: 2px;
                text-decoration: none;
                font-size: 18px;
                font-weight: bold;
                color: #d1d1d1;

                svg {
                    font-size: 18px;
                    font-weight: bold;
                    color: #d1d1d1;
                }

                &::after {
                        position: absolute;
                        width: 0;
                        height: 2px;
                        bottom: -5px;
                        left: 0;
                        content: " ";
                        background-color: #2200A8;
                        transition: 0.5s ease-in-out;
                }

                &:hover {
                    color:#2200A8;
                }

                &:hover::after {
                        width: 100%;
                    
                }

                &:hover svg {
                        color:#2200A8;
                    }

            }
        }

    }

    @media screen and (max-width: 768px) {
        display: none;
    }

    @media (max-width: 1310px) {
        ul.ul-logged-in {
            width: 50%;

            li {
                a {
                    font-size: 16px;
                }
            }
        }

        ul.ul-logged-out {
            width: 10%;

            li {
                a {
                    font-size: 16px;
                }
            }
        }
    } 

`;

export const Nav = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 50px;
    width: 65%;
    height: 100%;

    a {
        display: flex;
        justify-content: flex-start;
        height: 100%;
        align-items: baseline;
        text-decoration: none;
                
        img {
            width: 70px;
            height: 70px;
            margin-top: 8px;
        }

         h1 {
            font-family: 'Bebas Neue', sans-serif;
            font-weight: bold;
            color: #d1d1d1;
        }

    }

    @media (max-width: 1310px) {
        gap: 40px;

        &.nav-logged-in {
            width: 65%;
        }

        &.nav-logged-out {
            width: 100%;
        }
    } 


`;

export const InputContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding-right: 20px;
    border-radius: 10px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset, -8px 8px 16px 0px rgb(0, 0, 0);
    background-color: #1f1f23;

    input {
            width: 100%;
            height: 50px;
            padding: 0 20px;
            border: none;
            font-size: 16px;
            font-weight: bold;
            background-color: transparent;
            color: #d1d1d1;

            &:focus {
                outline: none;
            }
    }

    svg {
        color: #d1d1d1;
        font-size: 28px;
    }

`;

export const MobileMenu = styled.div`
    position: absolute;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 50%;
    height: 100%;
    top: 0px;
    left: 0;
    background-color: #0f0f12;
    z-index: 10000;

    ul {
        position: relative;
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        height: 100%;
        top: 70px;

        li {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            list-style: none;
            margin-top: 25px;
            color: #d4d4d8;

            a {
                width: 90%;
                text-decoration: none;
                text-align: start;
                font-size: 15px;
                font-weight: bold;
                color: #d4d4d8;

                svg.icon-plus {
                    margin-right: 5px;
                    position: inherit;
                    font-size: 18px;
                    color: #d4d4d8;
                }
            }

            
        }
        
        svg {
            position: absolute;
            top: 160px;
            left: 33%;
            font-size: 50px;
            color: #d4d4d8;
        }
    }

`;