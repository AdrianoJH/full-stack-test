import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:3000/api', // URL da sua API backend
});

// Função para configurar o token de autenticação em todas as requisições
export const setAuthToken = (token) => {
    if (token) {
        api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    } else {
        delete api.defaults.headers.common['Authorization'];
    }
};

// Funções para fazer requisições à API
export const registerUser = async (userData) => {
    try {
        const response = await api.post('/register', userData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const loginUser = async (userData) => {
    try {
        const response = await api.post('/login', userData);
        const { token } = response.data;
        setAuthToken(token);
        console.log("Login efetuado com sucesso!", response.data);
        return response.data;

    } catch (error) {
        throw error.response.data;
    }
};

export const logoutUser = async () => {
    try {
        const response = await api.post('/logout');
        return response.data;
    } catch (error) {
        throw new Error('Erro ao fazer logout: ' + error.message);
    }
};

export const getUserInfo = async () => {
    try {
        const response = await api.get('/user');
        console.log('Resposta da requisição /user:', response);
        if (response.status === 200 && response.data) {
            return response.data;
        } else {
            return null; // Usuário não autenticado
        }
    } catch (error) {
        throw error.response.data;
    }
};

export const updateUser = async (userData) => {
    try {
        const response = await api.put('/user', userData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const deleteUser = async () => {
    try {
        const response = await api.delete('/user'); // Rota para excluir usuário
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};


export const getMovies = async () => {
    try {
        const response = await api.get('/movies');
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const getMyMovies = async () => {
    try {
        const response = await api.get('/my-movies');
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const getMovie = async (id) => {
    try {
        const response = await api.get(`/movies/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const addMovie = async (formData) => {
    try {
        const response = await api.post('/movies', formData, {
            headers: {
                'Content-Type': 'multipart/form-data', // Importante para o envio de arquivos
            },
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const updateMovie = async (id, movieData) => {
    try {
        const formData = new FormData();
        // Adiciona os dados do filme ao FormData
        Object.keys(movieData).forEach(key => {
            if (key === 'image') {
                formData.append(key, movieData[key]);
            } else {
                formData.append(key, JSON.stringify(movieData[key]));
            }
        });

        const response = await api.put(`/movies/${id}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data' // Define o cabeçalho adequado para envio de imagens
            }
        });
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const deleteMovie = async (id) => {
    try {
        const response = await api.delete(`/movies/${id}`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const addMovieRating = async (movieId, ratingData) => {
    try {
        const response = await api.post(`/movies/${movieId}/ratings`, ratingData);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const getMovieRatings = async (movieId) => {
    try {
        const response = await api.get(`/movies/${movieId}/ratings`);
        return response.data;
    } catch (error) {
        throw error.response.data;
    }
};

export const hasUserRatedMovie = async (movieId) => {
    try {
        const response = await api.get(`/movies/${movieId}/ratings/me`);
        return response.data.hasRated;
    } catch (error) {
        throw error.response.data;
    }
};


export default api;
