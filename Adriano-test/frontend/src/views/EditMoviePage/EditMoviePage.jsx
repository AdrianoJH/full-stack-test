import React, { useState, useEffect, useRef } from 'react';
import { MdFileUpload } from "react-icons/md";
import { IoMdArrowDropdown } from "react-icons/io";
import { Container, Box, BoxForm, TextArea, Button } from './EditMoviePageStyle';
import { getMovie, updateMovie } from '../../services/api';
import { useNavigate, useParams } from 'react-router-dom';

const EditMoviePage = () => {
    const { id } = useParams();
    const fileInputRef = useRef(null);
    const [selectedFile, setSelectedFile] = useState(null);
    const [movieDetails, setMovieDetails] = useState(null);
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        title: '',
        category: '',
        description: '',
    });

    useEffect(() => {
        const fetchMovieDetails = async () => {
            try {
                const movieData = await getMovie(id);
                setMovieDetails(movieData);
                setFormData({
                    title: movieData.title,
                    category: movieData.category,
                    description: movieData.description,
                });
                setSelectedFile(movieData.image ? { name: movieData.image.split('/').pop() } : null);
            } catch (error) {
                console.error('Erro ao buscar os detalhes do filme:', error);
            }
        };

        fetchMovieDetails();
    }, [id]);

    const handleFileChange = (e) => {
        const file = e.target.files[0];
        if (file) {
            setSelectedFile(file);
        }
    };

    const handleFileButtonClick = () => {
        if (fileInputRef.current) {
            fileInputRef.current.click();
        }
    };

    const handleLabelClick = (e) => {
        e.preventDefault();
        handleFileButtonClick();
    };

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        try {
            const updatedMovieData = { ...formData };
            // Se houver uma nova imagem selecionada, adicione-a aos dados do filme
            if (selectedFile) {
                updatedMovieData.image = selectedFile;
            }
            await updateMovie(id, updatedMovieData);
            // Redirecionar para a página de detalhes do filme após a edição bem-sucedida
            navigate(`/movie-detail/${id}`);
        } catch (error) {
            console.error('Erro ao editar o filme:', error);
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    return (
        <Container>
            <Box>
                <form onSubmit={handleFormSubmit}>
                    <h1>Editar Filme</h1>
                    <BoxForm>
                        <input
                            type="text"
                            placeholder='Digite o nome do filme'
                            name="title"
                            value={formData.title}
                            onChange={handleInputChange}
                        />
                    </BoxForm>
                    <BoxForm>
                        <label htmlFor="fileImg" onClick={handleLabelClick}>
                            {selectedFile ? (
                                <>
                                    {selectedFile.name}
                                </>
                            ) : (
                                <>
                                    Selecione uma imagem
                                    <MdFileUpload />
                                </>
                            )}
                        </label>
                        <input
                            ref={fileInputRef}
                            type='file'
                            id='fileImg'
                            accept='image/*'
                            style={{ display: 'none' }}
                            onChange={handleFileChange}
                        />
                    </BoxForm>
                    <BoxForm>
                        <select
                            name="category"
                            value={formData.category}
                            onChange={handleInputChange}
                        >
                            <option value="">Selecionar</option>
                            <option value="Ação">Ação</option>
                            <option value="Drama">Drama</option>
                            <option value="Comédia">Comédia</option>
                            <option value="Ficção">Ficção</option>
                            <option value="Aventura">Aventura</option>
                            <option value="Terror">Terror</option>
                            <option value="Romance">Romance</option>
                        </select>
                        <span><IoMdArrowDropdown /></span>
                    </BoxForm>
                    <TextArea
                        rows="5"
                        placeholder='Descrição'
                        name="description"
                        value={formData.description}
                        onChange={handleInputChange}
                    ></TextArea>
                    <Button type="submit">Salvar Alterações</Button>
                </form>
            </Box>
        </Container>
    );
}

export default EditMoviePage;
