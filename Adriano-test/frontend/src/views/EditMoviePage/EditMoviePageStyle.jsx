import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;

    @media (max-width: 768px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        height: 100vh;
        overflow-x: hidden !important;
    }

`;

export const Box = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 30px;
    width: 350px;
    height: 520px;
    margin-top: 20px;
    padding: 30px;
    border-radius: 15px;
    background-color: #0f0f12;
    box-shadow: #111112 -10px 10px 40px;

    form {
        display: flex;
        align-items: center;
        flex-direction: column;
        gap: 30px;
        width: 100%;
    }

    h1 {
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
        &::after {
                    display: flex;
                    width: 120%;
                    height: 2px;
                    margin-top: 10px;
                    margin-left: -25px;
                    content: " ";
                    background-color: #2200A8;
        }
    }

    @media (max-width: 768px) {
        width: 85%;
        height: 100%;
        margin-top: 130px;
        margin-bottom: 50px;
        margin-left: 0;

        h1 {
            font-size: 25px;
            margin-top: -15px;
        }
    }

    @media (max-width: 1024px) {
        
    }   

`;

export const BoxForm = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    width: 100%;
    height: 40px;
    border-radius: 8px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
    background-color: #1f1f23;

    label {
        display: flex;
        align-items: center;
        overflow: hidden;
        gap: 10px;
        width: 290px;
        height: 100%;
        padding: 10px 10px;
        font-size: 16px;
        font-weight: bold;
        text-overflow: ellipsis !important;
        white-space: nowrap;
        color: #d1d1d1;
        cursor: pointer;

        svg {
            font-size: 20px;
            color: #d1d1d1;
        }

        &:hover {
                color:  #2200A8;
        }

        &:hover svg {
                color:  #2200A8;
        }
    }

    input {
        width: 100%;
            height: 100%;
            padding: 0 10px;
            font-size: 16px;
            font-weight: bold;
            border: none;
            color: #d1d1d1;
            background: transparent;

            &:focus {
                outline: none;
            }
    }

    input[type="file"] {
      display: none;
    }

    select {
        width: 100%;
        height: 100%;
        padding: 0 10px;
        font-size: 16px;
        font-weight: bold;
        border: none;
        color: #d1d1d1;
        background: transparent;
        appearance: none;
        cursor: pointer;

        &:focus {
            outline: none;
        }

        option {
            font-size: 16px;
            font-weight: bold;
            color: #d1d1d1;
            box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
            background-color: #1f1f23;

            &:active {
                background-color:  #2200A8 !important;
            }
        }
    }

    span {
        position: absolute;
        top: 7px;
        right: 5px;
        font-size: 20px;
        font-weight: bold;
        color: #d1d1d1;
        pointer-events: none;
        cursor: pointer;
    }
`;

export const TextArea = styled.textarea`
    width: 100%;
    padding: 10px 10px;
    border-radius: 8px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
    background-color: #1f1f23;
    font-size: 16px;
    font-weight: bold;
    border: none;
    color: #d1d1d1;
    background-color: #1f1f23;

    &:focus {
        outline: none;
    }


`;


export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 22px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    &:hover {
        background-color:  #2200A8;
    }

`;
