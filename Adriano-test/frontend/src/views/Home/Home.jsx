import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getMovies, getMovieRatings } from '../../services/api';
import { FaStar } from "react-icons/fa";
import { IoSearchOutline } from "react-icons/io5";
import { IoIosArrowDropleftCircle, IoIosArrowDroprightCircle } from "react-icons/io";
import Logo from '../../img/logo.svg';
import { Container, Content, BoxContent, Cards, PaginationContainer, PaginationButton, ButtonContainer, ButtonContainer1, Nav, InputContainer } from './HomeStyle';

const Home = ({ searchTerm, setSearchTerm, userId }) => { // Recebendo o searchTerm como uma propriedade
  const [movies, setMovies] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [moviesPerPage] = useState(8);

  useEffect(() => {
    const fetchMovies = async () => {
      try {
        const moviesData = await getMovies();

        const moviesWithRatings = await Promise.all(
          moviesData.map(async movie => {
            const ratings = await getMovieRatings(movie.id);
            const totalRating = ratings.reduce((acc, rating) => acc + rating.rating, 0);
            const averageRating = totalRating / ratings.length || 0;
            return { ...movie, averageRating };
          })
        );

        setMovies(moviesWithRatings);
      } catch (error) {
        console.error('Erro ao buscar os filmes:', error);
      }
    };

    fetchMovies();
  }, []);

  // Lógica para filtrar os filmes de acordo com o termo de busca
  const filteredMovies = movies.filter(movie => {
    // Verifique se searchTerm é uma string antes de chamar toLowerCase()
    if (typeof searchTerm === 'string') {
      return movie.title.toLowerCase().includes(searchTerm.toLowerCase());
    }
    return false;
  });

  const indexOfLastMovie = currentPage * moviesPerPage;
  const indexOfFirstMovie = indexOfLastMovie - moviesPerPage;
  const currentMovies = filteredMovies.slice(indexOfFirstMovie, indexOfLastMovie);

  const totalPages = Math.ceil(filteredMovies.length / moviesPerPage);
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <Container>
      <Nav>
        <Link to='/'>
          <img src={Logo} alt="logo" />
          <h1 className='title-logo'>MovieStar</h1>
        </Link>
        <InputContainer>
          <input type="text" placeholder='Buscar Filmes' onChange={(e) => setSearchTerm(e.target.value)} />
          <IoSearchOutline />
        </InputContainer>
      </Nav>
      <h1>Filmes</h1>
      <Content>
        <BoxContent>
          {currentMovies.map(movie => (
            <Cards key={movie.id}>
              <img src={`${movie.image}`} alt={movie.title} />
              <span><FaStar /> <p>{movie.averageRating.toFixed(1).replace(/\.0$/, '')}</p></span>
              <h4>{movie.title}</h4>
              <Link to={`/movie-detail/${movie.id}?userId=${userId}`} className='btnToAsses'>
                Avaliar
              </Link>
              <Link to={`/movie-detail/${movie.id}?userId=${userId}`} className='btnDetails'>
                Detalhes
              </Link>

            </Cards>
          ))}
        </BoxContent>
      </Content>
      <PaginationContainer>
        <ButtonContainer1>
          {Array.from({ length: totalPages }, (_, i) => (
            <PaginationButton
              key={i}
              onClick={() => paginate(i + 1)}
              active={currentPage === i + 1 ? true : undefined}
            >
              {i + 1}
            </PaginationButton>
          ))}
        </ButtonContainer1>
        <ButtonContainer>
          <PaginationButton onClick={() => paginate(currentPage - 1)} disabled={currentPage === 1}>
            <IoIosArrowDropleftCircle />
          </PaginationButton>
          <PaginationButton onClick={() => paginate(currentPage + 1)} disabled={indexOfLastMovie >= filteredMovies.length}>
            <IoIosArrowDroprightCircle />
          </PaginationButton>
        </ButtonContainer>
      </PaginationContainer>
    </Container>
  );
}

export default Home;
