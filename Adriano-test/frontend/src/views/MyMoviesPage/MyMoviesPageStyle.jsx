import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    gap: 40px;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: flex-start;
    padding: 30px;

    h1 {
        position: relative;
        margin-left: 10px;
        text-align: start;
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
        &::before {
                    position: absolute;
                    display: flex;
                    width: 3px;
                    height: 40px;
                    bottom: 5px;
                    left: -10px;
                    content: " ";
                    background-color: #2200A8;
        }
    }

    @media (max-width: 768px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        height: 100%;
        padding: 0;
        overflow-x: hidden !important;

        h1 {
            width: 88%;
        }

        h1.title-logo {
            &::before {
                width: 0;
            }
        }

    }

`;
export const Nav = styled.div`
    display: none;
    
    @media (max-width: 768px) {
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: center;
        gap: 20px;
        width: 90%;
        height: 100%;

        a {
            display: flex;
            justify-content: flex-start;
            height: 100%;
            align-items: baseline;
            text-decoration: none;
                    
            img {
                width: 70px;
                height: 70px;
                margin-top: 8px;
            }

            h1 {
                font-family: 'Bebas Neue', sans-serif;
                font-weight: bold;
                color: #d1d1d1;
            }

        }
    }

`;

export const InputContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding-right: 20px;
    border-radius: 10px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset, -8px 8px 16px 0px rgb(0, 0, 0);
    background-color: #1f1f23;

    input {
            width: 100%;
            height: 50px;
            padding: 0 20px;
            border: none;
            font-size: 16px;
            font-weight: bold;
            background-color: transparent;
            color: #d1d1d1;

            &:focus {
                outline: none;
            }
    }

    svg {
        color: #d1d1d1;
        font-size: 28px;
    }

`;

export const Content = styled.div`
    display: flex;
    justify-content: center;
    width: 100%;
    height: 100%;
    padding: 0 110px;

    @media (max-width: 768px) {
        padding: 0;
    }    

    @media (max-width: 1360px) {
        padding: 0 80px;
    }    
`;

export const BoxContent = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    gap: 20px;
    width: 100%;
    height: 100%;

    a.btnToAsses {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        min-height: 25px;
        background-color: #1f1f23;
        color: rgb(209, 209, 209);
        font-size: 14px;
        font-weight: bold;
        text-decoration: none;
        border: none;
        border-radius: 5px;
        box-shadow: #000000af 0px 10px 10px;
        cursor: pointer;

        &:hover {
            color: #1f1f23;
            background-color:  #d1d1d1;
        }
    }

    a.btnDetails {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        min-height: 25px;
        background-color: #2200A8;
        color: #d1d1d1;
        font-size: 14px;
        font-weight: bold;
        text-decoration: none;
        border: none;
        border-radius: 5px;
        box-shadow: #000000af 0px 10px 10px;
        cursor: pointer;

        &:hover {
            color: #2200A8;
            background-color:  #d1d1d1;
        }
    }

    @media (max-width: 768px) {
        justify-content: center;
    }   
`;

export const Cards = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 10px;
    width: 250px;
    height: 425px;
    padding: 30px;
    border-radius: 15px;
    background-color: #0f0f12;
    box-shadow: #111112 -10px 10px 40px;

    img {
        width: 190px;
        height: 250px;
        min-height: 250px;
    }

    span {
        display: flex;
        align-items: center;
        gap: 5px;
        font-size: 18px;
        color: #2200A8;
        
        p {
            margin-bottom: -7px;
            font-size: 14px;
            font-weight: bold;
            color: #d1d1d1;
        }
    }
    h4 {
        width: 190px;
        min-height: 20px;
        overflow: hidden;
        text-overflow: ellipsis;
        font-weight: bold;
        white-space: nowrap;
        color: #d1d1d1;
    }
   
`;

export const PaginationContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    gap: 50px;
    width: 100%;
    padding: 0 110px;
`;

export const ButtonContainer1 = styled.div`
    display: flex;
    justify-content: flex-end;
    gap: 10px;
`;

export const ButtonContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    gap: 25px;
`;

export const PaginationButton = styled.button`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 30px;
    height: 30px;
    padding: 0;
    border: none !important;
    border-radius: 50%;
    font-size: 18px;
    font-weight: bold;
    color: ${props => props.active ? '#d1d1d1' : '#1f1f23'};
    background-color: ${props => props.active ? '#0f0f12' : '#d1d1d1'};
    cursor: pointer;

    svg {
        position: absolute;
        font-size: 40px;
        color: #2200A8;

        &:hover {
            color: #180079;
        }
    }
`;

