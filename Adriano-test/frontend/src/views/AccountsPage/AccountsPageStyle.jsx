import styled from 'styled-components';

export const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
    padding-top: 45px;

    @media (max-width: 768px) {
        padding: 50px 0;
    }

`;

export const Content = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    height: 100%;
    justify-content: center;
    gap: 30px;
    padding: 30px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset, -8px 8px 16px 0px rgb(20, 20, 20);
    background-color: #4e4e51;

    @media (max-width: 768px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 0;
        width: 90%;
        height: 100%;
        padding: 0;
        padding-bottom: 20px;
    }

`;

export const ContentBox = styled.div`
    display: flex;
    height: 100%;
    width: 100%;
    justify-content: center;
    gap: 50px;

    @media (max-width: 768px) {
        flex-direction: column;
        align-items: center;
        gap: 0;
    }

`;

export const Box = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 30px;
    width: 350px;
    height: 100%;
    margin-top: 20px;
    padding: 30px;
    border-radius: 15px;
    background-color: #0f0f12;
    box-shadow: #111112 -10px 10px 40px;

    form {
        display: flex;
        align-items: center;
        flex-direction: column;
        gap: 30px;
        width: 100%;
    }

    h1 {
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
    }

    @media (max-width: 768px) {
        width: 90%;
        height: 100%;
        margin-left: 0;

        h1 {
            font-size: 25px;
            margin-top: -15px;
        }
    }

`;

export const InputContainer = styled.div`
    display: flex;
    align-items: center;
    gap: 10px;
    width: 100%;
    height: 40px;
    padding: 0 10px;
    border-radius: 8px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
    background-color: #1f1f23;

    svg {
        font-size: 18px;
        color: #d1d1d1;
        cursor: pointer;
        
        &.eye {
            font-size: 25px;
            color:  #2200A8;
            
            &:hover {
                color: #d1d1d1;
            }
        }
    }
    
`;

export const Input = styled.input`
    width: 100%;
    height: 100%;
    font-size: 16px;
    font-weight: bold;
    border: none;
    color: #d1d1d1;
    background: transparent;

    &:focus {
        outline: none;
    }

    &.custom-email {
        background-color: #000000;
        color: #747474;

    }
`;

export const BoxBtn = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;

    @media (max-width: 768px) {
        flex-direction: column;
        align-items: center;
        gap: 20px;
        width: 90%;
        margin-top: 20px;
    }
`;

export const ButtonDel = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 350px;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 22px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    &:hover {
        background-color:  #590101;
    }

    @media (max-width: 768px) {
       width: 100%;
    }

`;

export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 350px;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 22px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    &:hover {
        background-color:  #2200A8;
    }

    @media (max-width: 768px) {
       width: 100%;
    }

`;





