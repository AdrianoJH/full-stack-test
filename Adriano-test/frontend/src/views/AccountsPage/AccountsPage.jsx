import React, { useState, useEffect } from 'react';
import { FaEye, FaEyeSlash, FaLock } from "react-icons/fa";
import { getUserInfo, updateUser, deleteUser } from '../../services/api';
import ModalDel from '../../components/ModalDel/ModalDel';
import { Container, Content, ContentBox, Box, Input, BoxBtn, Button, ButtonDel, InputContainer } from './AccountsPageStyle';

const AccountsPage = ({ handleLogout }) => {
  const [showPassword, setShowPassword] = useState({
    password: false,
    confirmPassword: false
  });
  const [showModal, setShowModal] = useState(false); // Estado para controlar a exibição da modal
  const [userData, setUserData] = useState({
    name: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: ''
  });

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        const userInfo = await getUserInfo();
        setUserData(userInfo);
      } catch (error) {
        console.error('Erro ao buscar informações do usuário:', error);
      }
    };

    fetchUserInfo();
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (userData.password !== '') {
        if (userData.password === userData.confirmPassword) {
          await updateUser(userData);
          console.log('Dados do usuário atualizados com sucesso');
        } else {
          console.error('A senha e a confirmação de senha não coincidem');
        }
      } else {
        const { password, confirmPassword, ...updatedData } = userData;
        await updateUser(updatedData);
        console.log('Dados do usuário atualizados com sucesso');
      }
    } catch (error) {
      console.error('Erro ao atualizar os dados do usuário:', error);
    }
  };

  const openModal = () => {
    setShowModal(true); // Abre a modal
  };

  const handleDeleteUser = async () => {
    try {
      await deleteUser();
      handleLogout();
      console.log('Usuário excluído com sucesso');
      setShowModal(false); // Feche o modal após a exclusão do usuário
    } catch (error) {
      console.error('Erro ao excluir usuário:', error);
    }
  };

  return (
    <Container>
      <Content>
        <ContentBox>
          <Box>
            <h1>{userData.name}</h1>
            <form onSubmit={handleSubmit}>
              <InputContainer>
                <Input
                  type='text'
                  name='name'
                  placeholder='Nome'
                  value={userData.name}
                  onChange={handleChange}
                />
              </InputContainer>
              <InputContainer>
                <Input
                  type='text'
                  name='lastName'
                  placeholder='Sobrenome'
                  value={userData.lastName}
                  onChange={handleChange}
                />
              </InputContainer>
              <InputContainer>
                <Input
                  type='email'
                  name='email'
                  placeholder='E-mail'
                  value={userData.email}
                  onChange={handleChange}
                  disabled
                />
              </InputContainer>
            </form>
          </Box>
          <Box>
            <form>
              <h1>Alterar Senha</h1>
              <InputContainer>
                <FaLock />
                <Input
                  type='password'
                  name='password'
                  placeholder='Nova Senha'
                  value={userData.password}
                  onChange={handleChange}
                />
                {showPassword.password ? (
                  <FaEyeSlash onClick={() => setShowPassword(prevState => ({ ...prevState, password: false }))} className='eye' />
                ) : (
                  <FaEye onClick={() => setShowPassword(prevState => ({ ...prevState, password: true }))} className='eye' />
                )}
              </InputContainer>
              <InputContainer>
                <FaLock />
                <Input
                  type='password'
                  name='confirmPassword'
                  placeholder='Confirme a Nova Senha'
                  value={userData.confirmPassword}
                  onChange={handleChange}
                />
                {showPassword.confirmPassword ? (
                  <FaEyeSlash onClick={() => setShowPassword(prevState => ({ ...prevState, confirmPassword: false }))} className='eye' />
                ) : (
                  <FaEye onClick={() => setShowPassword(prevState => ({ ...prevState, confirmPassword: true }))} className='eye' />
                )}
              </InputContainer>
            </form>
          </Box>
        </ContentBox>
        <BoxBtn>
          <ButtonDel onClick={openModal}>Excluir Conta</ButtonDel>
          <Button type="submit" onClick={handleSubmit}>Salvar</Button>
        </BoxBtn>
      </Content>
      {/* Renderiza a modal se showModal for true */}
      {showModal && (
        <ModalDel
          showDeleteButton={true} // Define showDeleteButton como true para exibir o botão de exclusão
          onDelete={handleDeleteUser} // Passa a função de exclusão como propriedade
          setShowModal={setShowModal}
        />
      )}
    </Container>
  )
}

export default AccountsPage;
