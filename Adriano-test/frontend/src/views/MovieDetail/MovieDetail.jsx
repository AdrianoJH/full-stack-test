import React, { useState, useEffect, useRef } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { getMovie, getMovieRatings, deleteMovie } from '../../services/api';
import { Container, Content, BoxText, BoxImage, ContainerAvaliation, BoxBtnDetails } from './MovieDetailStyle';
import Avaliation from '../../components/Avaliation/Avaliation';
import { FaStar } from 'react-icons/fa';
import ModalDel from '../../components/ModalDel/ModalDel'; // Importe o componente ModalDel

const MovieDetail = ({ userId }) => {
    const { id } = useParams();
    const [movie, setMovie] = useState(null);
    const [averageRating, setAverageRating] = useState(null);
    const avaliationsRef = useRef(null);
    const navigate = useNavigate();
    const [showDeleteModal, setShowDeleteModal] = useState(false); // Estado para controlar a exibição do modal

    useEffect(() => {
        const fetchMovie = async () => {
            try {
                const movieData = await getMovie(id);
                setMovie(movieData);
                if (avaliationsRef.current) {
                    avaliationsRef.current.scrollIntoView({ behavior: 'smooth' });
                }
            } catch (error) {
                console.error('Erro ao buscar os detalhes do filme:', error);
            }
        };

        fetchMovie();
    }, [id]);

    useEffect(() => {
        const fetchAverageRating = async () => {
            try {
                const ratings = await getMovieRatings(id);
                const totalRatings = ratings.length;
                const sumRatings = ratings.reduce((acc, rating) => acc + rating.rating, 0);
                const average = totalRatings > 0 ? sumRatings / totalRatings : 0;
                setAverageRating(average);
            } catch (error) {
                console.error('Erro ao buscar a média de avaliação do filme:', error);
            }
        };

        fetchAverageRating();
    }, [id]);

    const handleDeleteMovie = async () => {
        try {
            await deleteMovie(id);
            // Redirecionar para a página inicial após a exclusão bem-sucedida
            navigate('/');
        } catch (error) {
            console.error('Erro ao excluir o filme:', error);
        }
    };

    const handleOpenDeleteModal = () => {
        setShowDeleteModal(true); // Define o estado para mostrar o modal de exclusão
    };

    const handleEditMovie = () => {
        navigate(`/edit-movie/${id}`);
    };

    return (
        <Container>
            {movie ? (
                <>
                    <Content>
                        <BoxText>
                            <h1>{movie.title}</h1>
                            <span>
                                <p>{movie.category}</p>
                                |
                                <p><FaStar /> {averageRating ? averageRating.toFixed(1).replace(/\.0$/, '') : '0'}</p>
                            </span>
                            <h3>Descrição</h3>
                            <p>{movie.description}</p>
                            <BoxBtnDetails>
                                {console.log(userId, movie.id)} {/* Adicione este console.log para verificar os valores */}
                                {userId === movie.id && (
                                    <>
                                        <button className='btn-edit' onClick={handleEditMovie}>Editar</button>
                                        <button className='btn-delete' onClick={handleOpenDeleteModal}>Excluir</button>
                                    </>
                                )}
                            </BoxBtnDetails>

                        </BoxText>
                        <BoxImage>
                            <img src={movie.image} alt={movie.title} />
                        </BoxImage>
                    </Content>
                </>
            ) : (
                <p>Carregando...</p>
            )}
            <ContainerAvaliation ref={avaliationsRef}>
                <Avaliation movieId={id} />
            </ContainerAvaliation>
            {showDeleteModal && ( // Renderiza o modal se showDeleteModal for true
                <ModalDel
                    showDeleteButton={true} // Define showDeleteButton como true para exibir o botão de exclusão
                    onDelete={handleDeleteMovie} // Passa a função de exclusão como propriedade
                    setShowModal={setShowDeleteModal}
                />
            )}
        </Container>
    );
}

export default MovieDetail;
