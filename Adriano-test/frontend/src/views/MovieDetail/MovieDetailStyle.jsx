import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    gap: 40px;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
    padding: 30px;

    @media (max-width: 768px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        height: 100%;
        padding: 50px 0;

    }

`;

export const Content = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 85%;
    height: 100%;
    gap: 50px;

    &::after {
                position: absolute;
                display: flex;
                width: 100%;
                height: 5px;
                bottom: -50px;
                left: 0;
                content: " ";
                background-color: #2200A8;
    }

    @media (max-width: 768px) {
        flex-direction: column;
        width: 90%;
    }
`;

export const BoxText = styled.div`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 20px;
    width: 50%;
    height: 100%;

    h1 {
        position: relative;
        margin-left: 10px;
        text-align: start;
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
        &::before {
                    position: absolute;
                    display: flex;
                    width: 3px;
                    height: 100%;
                    bottom: 5px;
                    left: -10px;
                    content: " ";
                    background-color: #2200A8;
        }
    }

    span {
        display: flex;
        align-items: baseline;
        gap: 10px;
        font-size: 22px;
        color: #d1d1d172;
        
        p {
            text-align: start;
            font-weight: 600;
            color: #d1d1d1;
            font-size: 16px;
            line-height: 20px;

            svg {
                    font-size: 18px;
                    color:  #2200A8
            }
        }
    }

    p {
        text-align: start;
        font-weight: 600;
        color: #d1d1d1;
        font-size: 16px;
        line-height: 20px;
    }

    

    h3 {
        position: relative;
        text-align: start;
        font-weight: bold;
        color: #d1d1d1;

        &::before {
                    position: absolute;
                    display: flex;
                    width: 100%;
                    height: 3px;
                    bottom: -5px;
                    left: 0;
                    content: " ";
                    background-color: #2200A8;
        }
    }

    @media (max-width: 768px) {
        width: 100%;
    }

`;

export const BoxBtnDetails = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 30px;
    width: 100%;
    margin-top: 120px;

    button.btn-edit {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 350px;
        height: 40px;
        background-color: #1f1f23;
        color: #d1d1d1;
        font-size: 22px;
        font-weight: bold;
        border: none;
        border-radius: 8px;
        box-shadow: #000000af 0px 10px 10px;
        cursor: pointer;

        &:hover {
            background-color:  #2200A8;
        }

    }

    button.btn-delete {
        display: flex;
    justify-content: center;
    align-items: center;
    width: 350px;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 22px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    &:hover {
        background-color:  #590101;
    }

    }

    @media (max-width: 768px) {
       flex-direction: column;
       margin-top: 10px;

       button.btn-edit {
        width: 100%;
       }

       button.btn-delete {
        width: 100%;
       }
    }

`;

export const BoxImage = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    width: 50%;
    height: 100%;

    img {
        width: 300px;
        height: 410px;
        margin-top: 40px;
    }

    @media (max-width: 768px) {
        justify-content: center;
        width: 100%;

        img {
            width: 100%;
            max-width: 300px;
        }
    }
`;

export const ContainerAvaliation = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100%;
    width: 100%;
`;