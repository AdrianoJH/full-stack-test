import React, { useState } from 'react';
import { Link, Navigate } from 'react-router-dom';
import { FaEye, FaEyeSlash, FaLock } from "react-icons/fa"; // Importe também o ícone de olho fechado
import { MdEmail } from "react-icons/md";
import { Container, Box, Input, Box2, Button } from './LoginPageStyle';
import { loginUser } from '../../services/api';

const LoginPage = ({ loggedIn, setUserName, setLoggedIn }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [showPassword, setShowPassword] = useState(false); // Estado para controlar a visibilidade da senha

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      // Chama a função para fazer login, passando os dados do formulário
      const response = await loginUser({ email, password });

      // Se o login for bem-sucedido, atualiza o estado para indicar que o usuário está logado
      if (response) {
        localStorage.setItem('token', response.token);
        setLoggedIn(true);
        setUserName();
        window.location.reload();
      }
    } catch (error) {
      // Se houver algum erro durante o login, exibe uma mensagem de erro
      setError('Usuário ou senha incorretos');
    }
  };

  // Se o usuário estiver logado, redireciona para a página principal
  if (loggedIn) {
    return <Navigate to="/" />;
  }

  return (
    <Container>
      <Box>
        <h1>Login</h1>
        {error && <p>{error}</p>}
        <form onSubmit={handleLogin}>
          <Input>
            <MdEmail />
            <input
              type="text"
              placeholder='E-mail'
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Input>

          <Input>
            <FaLock />
            <input
              type={showPassword ? 'text' : 'password'} // Altera o tipo de entrada com base na visibilidade da senha
              placeholder='Senha'
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            {showPassword ? ( // Mostra o ícone de olho aberto se a senha estiver visível
              <FaEyeSlash onClick={() => setShowPassword(false)} className='eye'/>
            ) : ( // Caso contrário, mostra o ícone de olho fechado
              <FaEye onClick={() => setShowPassword(true)} className='eye'/>
            )}
          </Input>
          <Box2>
            <p>Ainda não possui conta?</p>
            <p><Link to='/register'>cadastrar-se</Link></p>
          </Box2>
          <Button type="submit">Entrar</Button>
        </form>
      </Box>
    </Container>
  )
}

export default LoginPage;
