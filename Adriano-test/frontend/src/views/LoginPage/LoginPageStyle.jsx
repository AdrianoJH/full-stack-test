import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;

    @media (max-width: 768px) {
        padding: 50px 0;
    }

`;

export const Box = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 350px;
    height: 420px;
    margin-top: 60px;
    padding: 30px;
    border-radius: 15px;
    background-color: #0f0f12;
    box-shadow: #111112 -10px 10px 40px;

    h1 {
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
        text-align: center;

        &::after {
                    display: flex;
                    width: 200%;
                    height: 2px;
                    margin-top: 10px;
                    margin-left: -45px;
                    content: " ";
                    background-color: #2200A8;
        }
    }

    @media (max-width: 768px) {
        width: 90%;
        height: 380px;
        margin-top: 35px;
        margin-bottom: 50px;
        margin-left: 0;

        form {
            width: 100%;
        }

    }

`;

export const Input = styled.div`
    display: flex;
    align-items: center;
    gap: 10px;
    width: 100%;
    height: 40px;
    padding: 0 10px;
    margin-top: 30px;
    border-radius: 8px;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
    background-color: #1f1f23;

    svg {
        font-size: 18px;
        color: #d1d1d1;
        cursor: pointer;
        
        
        &.eye {
            font-size: 25px;
            color:  #2200A8;
            
            &:hover {
                color: #d1d1d1;
            }
        }
    }

    input {
            width: 100%;
            height: 100%;
            font-size: 16px;
            font-weight: bold;
            border: none;
            color: #d1d1d1;
            background: transparent;

            &:focus {
                outline: none;
            }
    }
`;


export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    margin-top: 50px;
    font-size: 22px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    &:hover {
        background-color:  #2200A8;
    }

    @media (max-width: 1024px) {
        margin-top: 40px;
}

`;

export const Box2 = styled.div`
    flex-direction: row;
    display: flex;
    width: 99%;
    margin-top: 10px;
    justify-content: space-between;
    
    p {
        font-size: 12px;
        color: #d1d1d1;

        :hover {
            color: #2200A8;
        }
    }

    a{
        text-decoration: none;
        font-size: 12px;
        color: #d1d1d1;

        :hover {
            color: #2200A8;
        }

    }
    
`;




