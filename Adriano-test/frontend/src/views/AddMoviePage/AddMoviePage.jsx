import React, { useRef, useState } from 'react';
import { MdFileUpload } from "react-icons/md";
import { IoMdArrowDropdown } from "react-icons/io";
import { Container, Box, BoxForm, TextArea, Button } from './AddMoviePageStyle';
import { addMovie } from '../../services/api';

const AddMoviePage = () => {
  const fileInputRef = useRef(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [description, setDescription] = useState('');

  const handleFileButtonClick = () => {
    fileInputRef.current.click();
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setSelectedFile(file);
    }
  };

  const handleLabelClick = (e) => {
    e.preventDefault();
    handleFileButtonClick();
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const formData = new FormData();
      formData.append('title', title);
      formData.append('category', category);
      formData.append('description', description);
      formData.append('image', selectedFile);

      const response = await addMovie(formData);
      console.log("Filme adicionado com sucesso!", response);
      // Limpa os campos após o envio bem-sucedido
      setTitle('');
      setCategory('');
      setDescription('');
      setSelectedFile(null);
    } catch (error) {
      console.error('Erro ao adicionar filme:', error);
      // Trate o erro conforme necessário (exibindo uma mensagem de erro, etc.)
    }
  };

  return (
    <Container>
      <Box>
        <form onSubmit={handleSubmit}>
          <h1>Adicionar Filme</h1>
          <BoxForm>
            <input
              type="text"
              placeholder='Digite o nome do filme'
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              required
            />
          </BoxForm>
          <BoxForm>
            <label htmlFor="fileImg" onClick={handleLabelClick}>
              {selectedFile ? (
                <>
                  {selectedFile.name}
                </>
              ) : (
                <>
                  Selecione uma imagem
                  <MdFileUpload />
                </>
              )}
            </label>
            <input
              type='file'
              id='fileImg'
              accept='image/*'
              onChange={handleFileChange}
              ref={fileInputRef}
              style={{ display: 'none' }}
              required
            />
          </BoxForm>
          <BoxForm>
            <select
              value={category}
              onChange={(e) => setCategory(e.target.value)}
              required
            >
              <option value="">Selecionar</option>
              <option value="Ação">Ação</option>
              <option value="Drama">Drama</option>
              <option value="Comédia">Comédia</option>
              <option value="Ficção">Ficção</option>
              <option value="Aventura">Aventura</option>
              <option value="Terror">Terror</option>
              <option value="Romance">Romance</option>
            </select>
            <span><IoMdArrowDropdown /></span>
          </BoxForm>
          <TextArea
            rows="5"
            placeholder='Descrição'
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          ></TextArea>
          <Button type="submit">Adicionar</Button>
        </form>
      </Box>
    </Container>
  )
}

export default AddMoviePage;
