import React, { useState } from 'react';
import { Container, Box, Input, Button } from './RegisterPageStyle';
import { registerUser } from '../../services/api';
import { Navigate } from 'react-router-dom';

const RegisterPage = () => {
  const [email, setEmail] = useState('');
  const [userName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [error, setError] = useState('');
  const [registered, setRegistered] = useState(false);

  const handleRegister = async (e) => {
    e.preventDefault();

    // Verifique se as senhas correspondem
    if (password !== confirmPassword) {
      setError('As senhas não correspondem');
      return;
    }

    try {
      // Chama a função para registrar o usuário, passando os dados do formulário
      const userData = { email, userName, lastName, password };
      const response = await registerUser(userData);

      // Exibir mensagem de sucesso ou redirecionar o usuário para a página de login após o registro bem-sucedido
      console.log('Usuário registrado com sucesso:', response);
      setRegistered(true); // Defina o estado de registro como verdadeiro para ativar o redirecionamento
    } catch (error) {
      // Se houver algum erro durante o registro, exiba uma mensagem de erro
      setError('Erro ao registrar usuário: ' + error.message);
    }
  };

  if (registered) {
    // Se o registro for bem-sucedido, redirecione o usuário para a página de login
    return <Navigate to="/login" />;
  }

  return (
    <Container>
        {error && <p>{error}</p>}
      <Box>
        <h1>Criar Conta</h1>
        <form onSubmit={handleRegister}>
          <Input
            type='text'
            placeholder='E-mail'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <Input
            type='text'
            placeholder='Nome'
            value={userName}
            onChange={(e) => setFirstName(e.target.value)}
            required
          />
          <Input
            type='text'
            placeholder='Sobrenome'
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            required
          />
          <Input
            type='password'
            placeholder='Senha'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <Input
            type='password'
            placeholder='Confirme a Senha'
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            required
          />
          <Button type="submit">Cadastrar</Button>
        </form>
      </Box>
    </Container>
  )
}

export default RegisterPage;
