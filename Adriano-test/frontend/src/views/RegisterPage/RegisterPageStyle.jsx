import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;

    @media (max-width:1024px) {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        height: 100%;
        overflow-x: hidden !important;
    }

`;

export const Box = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 30px;
    width: 350px;
    height: 520px;
    margin-top: 20px;
    padding: 30px;
    border-radius: 15px;
    background-color: #0f0f12;
    box-shadow: #111112 -10px 10px 40px;

    form {
        display: flex;
        align-items: center;
        flex-direction: column;
        gap: 30px;
        width: 100%;
    }

    h1 {
        font-weight: bold;
        color: #d1d1d1;
        text-shadow: #000000af 0px 10px 10px;
        text-align: center;

        &::after {
                    display: flex;
                    width: 150%;
                    height: 2px;
                    margin-top: 10px;
                    margin-left: -45px;
                    content: " ";
                    background-color: #2200A8;
        }
    }

    @media (max-width: 768px) {
        width: 90%;
        height: 100%;
        margin-top: 55px;
        margin-bottom: 50px;
        margin-left: 0;

        h1 {
            &::after {
                width: 150%;
            }
        }

        form {
            width: 100%;
        }

    }


`;

export const Input = styled.input`
    width: 100%;
    height: 40px;
    padding: 0 10px;
    border: none;
    border-radius: 8px;
    font-size: 16px;
    font-weight: bold;
    color: #d1d1d1;
    box-shadow: -4px 8px 4px 0px rgba(0, 0, 0, 0.4) inset, -16px -64px 250px 0px rgba(40, 40, 41, 0.616) inset;
    background-color: #1f1f23;
`;


export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 40px;
    background-color: #1f1f23;
    color: #d1d1d1;
    font-size: 22px;
    font-weight: bold;
    border: none;
    border-radius: 8px;
    box-shadow: #000000af 0px 10px 10px;
    cursor: pointer;

    &:hover {
        background-color:  #2200A8;
    }

`;





