import React, { useState, useEffect } from 'react';
import { Container } from './styles/AppStyle';
import Header from './components/Header/Header';
import Routers from './routes/routes';
import { getUserInfo, setAuthToken } from './services/api';
import { useNavigate } from 'react-router-dom';

const App = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [userName, setUserName] = useState('');
  const [userId, setUserId] = useState(null);
  const [loggingOut, setLoggingOut] = useState(false); 
  const [searchTerm, setSearchTerm] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setAuthToken(token);
      getUserInfo().then(userInfo => {
        if (userInfo) {
          setLoggedIn(true);
          setUserName(userInfo.name);
          setUserId(userInfo.id);
        } else {
          handleLogout(); // Se não houver informações do usuário, faça logout
        }
      }).catch(error => {
        console.error('Erro ao verificar a autenticação do usuário:', error);
        handleLogout();
      });
    }
  }, []);

  useEffect(() => {
    if (loggingOut && !loggedIn) {
      // Se o usuário estiver fazendo logout e já estiver deslogado, redirecione para a página inicial
      navigate('/');
      setLoggingOut(false);
    }
  }, [loggingOut, loggedIn, navigate]);

  const handleLogout = () => {
    localStorage.removeItem('token');
    setLoggedIn(false);
    setUserName('');
    setUserId(null);
    setLoggingOut(true); // Ative o estado de logout
  };

  return (
    <Container>
      <Header loggedIn={loggedIn} userName={userName} handleLogout={handleLogout} setSearchTerm={setSearchTerm} />
      <Routers loggedIn={loggedIn} setUserName={setUserName} setLoggedIn={setLoggedIn} userId={userId} searchTerm={searchTerm} setSearchTerm={setSearchTerm} handleLogout={handleLogout}/>
    </Container>
  );
};

export default App;
